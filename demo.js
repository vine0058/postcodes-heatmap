
var codes = ['5000', '5000', '5000', '5038', '5038', '5027', '5034'];
var codedict = {};
var max = 0, mid1, mid2;
var map;
//MakeMap();


// File reader modified from: https://stackoverflow.com/questions/23331546/how-to-use-javascript-to-read-local-text-file-and-read-line-by-line#23332116
document.getElementById('file').onchange = function(){
  var file = this.files[0];
  var reader = new FileReader();
  reader.onload = function(progressEvent){
    // By lines
    codes = this.result.split('\n');
    document.getElementById('map').style.display = 'initial';
    document.getElementById('setup').style.display = 'none';
    MakeMap ();
  };
  reader.readAsText(file);
};
//

function Example() {
    document.getElementById('map').style.display = 'initial';
    document.getElementById('setup').style.display = 'none';
    MakeMap ();
}

function ChooseStyle(suburb) {
    var sty = {
        "color": "#ffffff",
        "weight": 1,
        "fillOpacity" : 0,
        "opacity": 0
    };
    
    if (codes.includes(suburb.properties.POSTCODE)) {
        sty["fillOpacity"] = 0.8;
        sty["opacity"] = 0.8;
        sty["color"] = "#ffcccc";
        
        if (codedict[suburb.properties.POSTCODE] > 1 ) 
            sty["color"] = "#ff9999";
        
        if (codedict[suburb.properties.POSTCODE] > mid1 ) 
            sty["color"] = "#ff6666";
            
        if (codedict[suburb.properties.POSTCODE] > mid2 ) 
            sty["color"] = "#ff3333";
            
        if (codedict[suburb.properties.POSTCODE] >= max ) 
            sty["color"] = "#ff0000";

    }
    return sty;
}

function MakeMap () {
    map = L.map('map');
    
    var layer = Tangram.leafletLayer({
        scene: 'scene.yaml',
        attribution: '<a href="https://mapzen.com/tangram" target="_blank">Tangram</a> | &copy; OSM contributors | <a href="https://mapzen.com/" target="_blank">Mapzen</a>'
    });

    layer.addTo(map);     
    map.setView([-34.9248994, 138.5850312], 11); 
    
    var i;
    for (i = 0; i < codes.length; i++) {
        if (codedict[codes[i]] != null) {
            codedict[codes[i]] = codedict[codes[i]] + 1;
            if (codedict[codes[i]] > max)
                max = codedict[codes[i]];
        } else {
            codedict[codes[i]] = 1;
        }
    }
    mid1 = 1 + 0.33 * (max - 1);
    mid2 = 1 + 0.66 * (max - 1);
    
    new L.GeoJSON.AJAX('Suburbs.geojson', {style: function (feature) {return ChooseStyle(feature);} }).addTo(map);
}

